# Start
`npm install --global windows-build-tools`
`npm run start`

# Requirement to use
nodeJs.
Game running in fullscreen 1920x1080.
A working numpad.
QWERTY layout.

# Usage
Use it at your own risks, it's just made for fun.
All commands are using the numpad.

Press `1` to start recording movements, press `1` again to stop recording.
Press `4` to repeat movements, auto flee if there is no luma. Auto movements are a bit buggy.

Alternatively, press `2` to auto flee if there is no luma, but without automating movements.