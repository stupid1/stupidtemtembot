const robot = require("robotjs");
const ioHook = require('iohook');
const { program } = require('commander');
const screenshot = require('screenshot-desktop');
const fs = require('fs');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');

const screenshotDir = 'screenshots/';
const ressourceDir = 'ressources/';

if (!fs.existsSync(screenshotDir)){
    fs.mkdirSync(screenshotDir);
}

const blackImg = PNG.sync.read(fs.readFileSync(ressourceDir + 'black.png'));
const barsImg = PNG.sync.read(fs.readFileSync(ressourceDir + 'bars.png'));
const lumaImg = toBW(PNG.sync.read(fs.readFileSync(ressourceDir + 'luma.png')));

program
  .option('-s, --screen <type>', 'select used screen', 0);

program.parse(process.argv);

const options = program.opts();
let displayId = null;

screenshot.listDisplays().then(displays => {
    displayId = displays[options.screen].id;
}).catch(err => {
    console.error(err);
});

let state = {
  running: false,
  recording: false,
}

let isInBattle = false;

let record = [];
let recordTimer = 0;
let keyPressed = new Set();

let lastSwitchDate = 0;
let alreadyAnalyzing = false;
let repeatRecord = false;

function clearRecord() {
    if (!record.length)
        return;

    keyPressed.forEach(key => record.push({ key: key, time: Date.now() - recordTimer, type: "keyup" }))

    const startTime = record[0].time;

    record.forEach(value => value.time = value.time - startTime);
}

function getSubImage(input, x, y, width, height) {
    let output = new PNG({ width, height });
    for (var j = 0; j < height; j++) {
        for (var i = 0; i < width; i++) {
            var outIdx = (width * j + i) << 2;
            var intIdx = (input.width * (j + y) + (i + x)) << 2;

            output.data[outIdx + 0] = input.data[intIdx + 0];
            output.data[outIdx + 1] = input.data[intIdx + 1];
            output.data[outIdx + 2] = input.data[intIdx + 2];
            output.data[outIdx + 3] = input.data[intIdx + 3];
        }
    }
    return output;
}

function toBW(input) {
    let output = new PNG({ width: input.width, height: input.height });
    for (var j = 0; j < input.height; j++) {
        for (var i = 0; i < input.width; i++) {
            var idx = (input.width * j + i) << 2;

            let sum = input.data[idx + 0] + input.data[idx + 1] + input.data[idx + 2];
            sum = sum / 3;
            if (input.data[idx + 3] == 0 || sum < 7 * 255 / 8)
                sum = 0;
            else
                sum = 255;

            output.data[idx + 0] = sum;
            output.data[idx + 1] = sum;
            output.data[idx + 2] = sum;
            output.data[idx + 3] = 255;
        }
    }
    return output;
}

function findImage(input, logo) {
    let incr = 0;
    for (var i = 0; i <= input.width - logo.width; i++) {
        for (var j = 0; j <= input.height - logo.height; j++) {
            const subImage = getSubImage(input, i, j, logo.width, logo.height);
            //fs.writeFileSync(screenshotDir + i + "_" + j + ".png", PNG.sync.write(subImage));
            //fs.writeFileSync(screenshotDir + i + "_" + j + "_logo.png", PNG.sync.write(logo));

            let diff = new PNG({ width: logo.width, height: logo.height });

            let numDiffPixels = pixelmatch(subImage.data, logo.data, diff.data, logo.width, logo.height, {threshold: 0.1});
            //fs.writeFileSync(screenshotDir + i + "_" + j + "_diff.png", PNG.sync.write(diff));
            if (numDiffPixels <= logo.width * logo.height / 8)
                incr++;
        }
    }
    //console.log(incr);
    return incr > 0;
}

function findNumberOfTem(pngScreen) {
    let numFound = 0;

    const { width, height } = barsImg;
    let tem1Bars = getSubImage(pngScreen, 1620, 115, width, height);
    if (findImage(tem1Bars, barsImg)) {
        numFound++;
        let tem2Bars = getSubImage(pngScreen, 1224, 60, width, height);
        if (findImage(tem2Bars, barsImg)) {
            numFound++;
        }
    }

    if (!!numFound) {
        const date = new Date();
        fs.writeFileSync(screenshotDir + date.getDay() + '_' + date.getMonth() + '_' + date.getHours() + '_'+ date.getMinutes() + '_'+ date.getSeconds() + '.png', PNG.sync.write(pngScreen));
    }

    return numFound;
}

function findLuma(pngScreen, numberOfTem) {
    if (numberOfTem < 1)
        return false;

    let tem1Name = toBW(getSubImage(pngScreen, 1565, 70, 205, 45));
    if (findImage(tem1Name, lumaImg))
        return true;

    if (numberOfTem > 1) {
        let tem2Name = toBW(getSubImage(pngScreen, 1170, 20, 205, 45));
        if (findImage(tem2Name, lumaImg))
            return true;
    }

    return false;
}

function continueRecord() {
    let currentTime = Date.now() - recordTimer;
    let newKeyPressed = new Set();

    for (let i = 0; i < record.length; i++) {
        if (record[i].time > currentTime)
            continue;

        let key = "";
        if (record[i].key == 17)
            key = "w";
        else if (record[i].key == 30)
            key = "a";
        else if (record[i].key == 31)
            key = "s";
        else if (record[i].key == 32)
            key = "d";

        if (record[i].type == "keydown")
            newKeyPressed.add(key);
        else if (record[i].type == "keyup")
            newKeyPressed.delete(key);
    }

    newKeyPressed.forEach(key => {
        if (!keyPressed.has(key)) {
            robot.keyToggle(key, "down");
            console.log("Down: " + key);
        }
    });

    keyPressed.forEach(key => {
        if (!newKeyPressed.has(key)) {
            robot.keyToggle(key, "up");
            console.log("Up: " + key);
        }
    });

    keyPressed = newKeyPressed;

    if (currentTime > record[record.length - 1].time) {
        recordTimer = Date.now();
        keyPressed.clear();
    }
}

function leaveGame() {
    robot.keyTap("f4", "alt");
    console.log("pressed: alt+f4");
    process.exit(1);
}

function leaveBattle() {
    robot.keyTap("8");
    console.log("pressed: 8");
    robot.keyTap("8");
    console.log("pressed: 8");
}

//ioHook.on("keydown", msg => { console.log(msg); });

//ioHook.on("keyup", msg => { console.log(msg); });


ioHook.on("keyup", keyPress => {
    // numpad 1
    if (keyPress.keycode === 79) {
        if (state.running) {
            console.error("Can't record while running");
            return;
        }

        state.recording = !state.recording;

        if (state.recording) {
            console.log("Start recording");

            keyPressed.clear();
            record = [];
            recordTimer = Date.now();
        } else {
            console.log("Stop recording");

            clearRecord();

            console.log(record);
        }
    }

    // numpad 2
    if (keyPress.keycode === 80) {
        if (state.recording) {
            console.error("Can't run while recording");
            return;
        }

        state.running = !state.running;

        if (state.running) {
            console.log("Start running");
            lastSwitchDate = Date.now();
            isInBattle = false;
            alreadyAnalyzing = false;
            recordTimer = Date.now();
            keyPressed.clear();
        } else {
            console.log("Stop running");
            repeatRecord = false;
            robot.keyToggle("w", "up");
            robot.keyToggle("a", "up");
            robot.keyToggle("s", "up");
            robot.keyToggle("d", "up");
        }
    }

    // numpad 4
    if (keyPress.keycode === 75) {
        if (state.recording) {
            console.error("Can't run while recording");
            return;
        }

        state.running = !state.running;

        if (state.running) {
            console.log("Start running");
            lastSwitchDate = Date.now();
            isInBattle = false;
            alreadyAnalyzing = false;
            recordTimer = Date.now();
            keyPressed.clear();
            repeatRecord = true;
        } else {
            console.log("Stop running");
            repeatRecord = false;
            robot.keyToggle("w", "up");
            robot.keyToggle("a", "up");
            robot.keyToggle("s", "up");
            robot.keyToggle("d", "up");
        }
    }
});

ioHook.on("keydown", msg => {
    if (!state.recording)
        return;

    if (msg.keycode != 17 && msg.keycode != 30 && msg.keycode != 31 && msg.keycode != 32)
        return;

    if (keyPressed.has(msg.keycode))
        return;

    keyPressed.add(msg.keycode);
    record.push({ key: msg.keycode, time: Date.now() - recordTimer, type: msg.type });
});

ioHook.on("keyup", msg => {
    if (!state.recording)
        return;

    if (msg.keycode != 17 && msg.keycode != 30 && msg.keycode != 31 && msg.keycode != 32)
        return;

    if (!keyPressed.has(msg.keycode))
        return;

    keyPressed.delete(msg.keycode);
    record.push({ key: msg.keycode, time: Date.now() - recordTimer, type: msg.type });
});

ioHook.start();

setInterval(() => {
    if (!state.running)
        return;

    if (Date.now() - lastSwitchDate > 300000) {
        console.error("Stuck somewhere for more than 5min");
        leaveBattle();
    }

    if (Date.now() - lastSwitchDate > 1800000) {
        console.error("Stuck somewhere for more than 30min");
        leaveGame();
    }

    screenshot({ format: 'png', screen: displayId }).then(screen => {
        if (alreadyAnalyzing)
            return;

        alreadyAnalyzing = true;

        let pngScreen = PNG.sync.read(screen)

        console.log("Finding Temtem...");
        let numberOfTem = findNumberOfTem(pngScreen);
        console.log(numberOfTem + " Temtem found");

        if (!!numberOfTem) {
            console.log("Finding Luma...");
            let lumaFound = findLuma(pngScreen, numberOfTem);
            if (!!lumaFound) {
                console.log("Luma found");
                leaveGame();
            } else {
                console.log("No Luma found");
                leaveBattle();
            }
            lastSwitchDate = Date.now();
        }

        alreadyAnalyzing = false;

    }).catch(err => {
        console.error(err);
    })

    if (repeatRecord && !alreadyAnalyzing)
        continueRecord();
}, 500);


// 8 => 9
// w => 17
// a => 30
// d => 32
// s => 31