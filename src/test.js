const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const fs = require('fs');

const ressourceDir = 'ressources/';
const testDir = 'tests/';
const screenshotDir = 'screenshots/';

const barsImg = PNG.sync.read(fs.readFileSync(ressourceDir + 'bars.png'));
const lumaImg = toBW(PNG.sync.read(fs.readFileSync(ressourceDir + 'luma.png')));
const simpleImg = PNG.sync.read(fs.readFileSync(testDir + 'simple.png'));
const luma1Img = PNG.sync.read(fs.readFileSync(testDir + 'luma1.png'));
const luma2Img = PNG.sync.read(fs.readFileSync(testDir + 'luma2.png'));

function getSubImage(input, x, y, width, height) {
    let output = new PNG({ width, height });
    for (var j = 0; j < height; j++) {
        for (var i = 0; i < width; i++) {
            var outIdx = (width * j + i) << 2;
            var intIdx = (input.width * (j + y) + (i + x)) << 2;

            output.data[outIdx + 0] = input.data[intIdx + 0];
            output.data[outIdx + 1] = input.data[intIdx + 1];
            output.data[outIdx + 2] = input.data[intIdx + 2];
            output.data[outIdx + 3] = input.data[intIdx + 3];
        }
    }
    return output;
}

function toBW(input) {
    let output = new PNG({ width: input.width, height: input.height });
    for (var j = 0; j < input.height; j++) {
        for (var i = 0; i < input.width; i++) {
            var idx = (input.width * j + i) << 2;

            let sum = input.data[idx + 0] + input.data[idx + 1] + input.data[idx + 2];
            sum = sum / 3;
            if (input.data[idx + 3] == 0 || sum < 7 * 255 / 8)
                sum = 0;
            else
                sum = 255;

            output.data[idx + 0] = sum;
            output.data[idx + 1] = sum;
            output.data[idx + 2] = sum;
            output.data[idx + 3] = 255;
        }
    }
    return output;
}

function findImage(input, logo) {
    let incr = 0;
    for (var i = 0; i < input.width - logo.width; i++) {
        for (var j = 0; j < input.height - logo.height; j++) {
            const subImage = getSubImage(input, i, j, logo.width, logo.height);
            //fs.writeFileSync(screenshotDir + i + "_" + j + ".png", PNG.sync.write(subImage));
            //fs.writeFileSync(screenshotDir + i + "_" + j + "_logo.png", PNG.sync.write(logo));

            let diff = new PNG({ width: logo.width, height: logo.height });

            let numDiffPixels = pixelmatch(subImage.data, logo.data, diff.data, logo.width, logo.height, {threshold: 0.1});
            //fs.writeFileSync(screenshotDir + i + "_" + j + "_diff.png", PNG.sync.write(diff));
            if (numDiffPixels <= logo.width * logo.height / 8)
                incr++;
        }
    }
    console.log(incr);
    return incr > 0;
}

const { width, height } = barsImg;
let temBars = getSubImage(simpleImg, 110, 705, width, height);

let diffPng = new PNG({ width, height });
const numDiffPixels = pixelmatch(temBars.data, barsImg.data, diffPng.data, width, height, {includeAA: false, threshold: 1});

// fs.writeFileSync(screenshotDir + "test.png", PNG.sync.write(temBars));
// fs.writeFileSync(screenshotDir + "diff.png", PNG.sync.write(diffPng));
// fs.writeFileSync(screenshotDir + "bars.png", PNG.sync.write(barsImg));

if (numDiffPixels <= width * height / 4)
    console.log("Temtem found");
else
    console.log("Temtem not found");

let temName = toBW(getSubImage(simpleImg, 55, 660, 205, 45));

fs.writeFileSync(screenshotDir + "name.png", PNG.sync.write(temName));
fs.writeFileSync(screenshotDir + "luma.png", PNG.sync.write(lumaImg));

if (findImage(temName, lumaImg))
    console.log("Luma found");
else
    console.log("Luma not found");

let temName2 = toBW(getSubImage(simpleImg, 1565, 70, 205, 45));
if (findImage(temName2, lumaImg))
    console.log("Luma2 found");
else
    console.log("Luma2 not found");

let temName3 = toBW(getSubImage(luma1Img, 1565, 70, 205, 45));

fs.writeFileSync(screenshotDir + "name3.png", PNG.sync.write(temName3));

if (findImage(temName3, lumaImg))
    console.log("Luma3 found");
else
    console.log("Luma3 not found");

let temName4 = toBW(getSubImage(luma2Img, 1170, 20, 205, 45));
if (findImage(temName4, lumaImg))
    console.log("Luma4 found");
else
    console.log("Luma4 not found");

let temName5 = toBW(getSubImage(luma2Img, 1565, 70, 205, 45));
if (findImage(temName5, lumaImg))
    console.log("Luma5 found");
else
    console.log("Luma5 not found");